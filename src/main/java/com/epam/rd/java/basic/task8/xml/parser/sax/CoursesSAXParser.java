package com.epam.rd.java.basic.task8.xml.parser.sax;

import com.epam.rd.java.basic.task8.courses.Course;
import com.epam.rd.java.basic.task8.xml.parser.AbstractCoursesXMLParser;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CoursesSAXParser extends AbstractCoursesXMLParser {
    protected static final SAXParserFactory SAX_PARSER_FACTORY = SAXParserFactory.newInstance();

    static {
        SAX_PARSER_FACTORY.setNamespaceAware(true);
    }

    @Override
    public List<Course> parse(InputStream xmlInputStream)
            throws IOException, XMLStreamException, ParserConfigurationException, SAXException {
        SAXParser parser = SAX_PARSER_FACTORY.newSAXParser();
        CoursesSAXHandler handler = new CoursesSAXHandler();
        parser.parse(xmlInputStream, handler);

        return handler.courses();
    }
}
