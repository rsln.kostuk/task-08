package com.epam.rd.java.basic.task8.xml.parser.sax;

import static com.epam.rd.java.basic.task8.xml.CoursesConstants.*;

import com.epam.rd.java.basic.task8.courses.Course;
import com.epam.rd.java.basic.task8.courses.Discipline;
import com.epam.rd.java.basic.task8.courses.Person;
import com.epam.rd.java.basic.task8.xml.CoursesConstants;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;

public class CoursesSAXHandler extends DefaultHandler {
    private List<Course> courses;
    private Course currentCourse;

    private String currentDisciplineName;
    private int currentDisciplinePractices = -1;
    private Discipline.FinalControl currentDisciplineControl;

    private String currentAuthorName;
    private String currentAuthorEmail;

    private final StringBuilder currentText = new StringBuilder();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch(qName) {
            case CoursesConstants.TARGET_NAMESPACE + ":" + ElementNames.Course.COURSES:
                    courses = new ArrayList<>();
                    break;
            case ElementNames.Course.COURSE:
                currentCourse = new Course();
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch(qName) {
            case ElementNames.Course.NAME:
                    currentCourse.setName(getTrimmedCurrentText());
                    break;
            case ElementNames.Course.START_TIME:
                    currentCourse.setStartTime(LocalDateTime.parse(getTrimmedCurrentText()));
                    break;
            case ElementNames.Course.END_TIME:
                    currentCourse.setEndTime(LocalDateTime.parse(getTrimmedCurrentText()));
                    break;
            case ElementNames.Discipline.NAME:
                    currentDisciplineName = getTrimmedCurrentText();
                    break;
            case ElementNames.Discipline.PRACTICES_COUNT:
                    currentDisciplinePractices = Integer.parseInt(getTrimmedCurrentText());
                    break;
            case ElementNames.Discipline.FINAL_CONTROL:
                    currentDisciplineControl = Discipline.FinalControl.valueOf(getTrimmedCurrentText().toUpperCase());
                    break;
            case ElementNames.Discipline.DISCIPLINE:
                    currentCourse.addDiscipline(
                        new Discipline(currentDisciplineName, currentDisciplinePractices, currentDisciplineControl));
                    currentDisciplineName = null;
                    currentDisciplinePractices = -1;
                    currentDisciplineControl = null;
                    break;
            case ElementNames.Author.NAME:
                    currentAuthorName = getTrimmedCurrentText();
                    break;
            case ElementNames.Author.EMAIL:
                    currentAuthorEmail = getTrimmedCurrentText();
                    break;
            case ElementNames.Author.AUTHOR:
                    currentCourse.addAuthor(new Person(currentAuthorName, currentAuthorEmail));
                    break;
            case ElementNames.Course.COURSE:
                    courses.add(currentCourse);
                    break;
        }
        currentText.setLength(0);
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentText.append(ch, start, length);
    }

    public List<Course> courses() {
        return courses;
    }

    private String getTrimmedCurrentText() {
        return currentText.toString().trim();
    }
}
