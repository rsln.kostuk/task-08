package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.courses.Course;
import com.epam.rd.java.basic.task8.xml.parser.CoursesXMLParser;
import com.epam.rd.java.basic.task8.xml.parser.dom.CoursesDOMParser;
import com.epam.rd.java.basic.task8.xml.parser.sax.CoursesSAXParser;
import com.epam.rd.java.basic.task8.xml.parser.stax.CoursesStAXParser;
import com.epam.rd.java.basic.task8.xml.validation.Validation;
import com.epam.rd.java.basic.task8.xml.writer.CoursesStAXWriter;
import com.epam.rd.java.basic.task8.xml.writer.XMLFormatter;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;

public class Main {
	static final String SCHEMA_FILE_PATH = "input.xsd";

	static void sort(List<Course> courses, Comparator<Course> comparator) {
		courses.sort(comparator);
	}

	static void runExample(CoursesXMLParser parser, Path inputXmlFilePath, Path xmlOutputFilePath,
						   Comparator<Course> courseComparator)
			throws IOException, XMLStreamException, ParserConfigurationException, SAXException, TransformerException {
		try(var xmlInputStream = Files.newInputStream(inputXmlFilePath);
			var xmlOutputStream = Files.newOutputStream(xmlOutputFilePath)) {
			var courses = parser.parse(xmlInputStream);
			sort(courses, courseComparator);
			writeFormattedCoursesXML(xmlOutputStream, courses);
		}
		Validation.validate(new File(xmlOutputFilePath.toString()), new File(SCHEMA_FILE_PATH));
	}

	static void writeFormattedCoursesXML(OutputStream outputStream, List<Course> courses)
			throws IOException, XMLStreamException, TransformerException {
		var pipedInputStream = new PipedInputStream();
		try(var pipedOutputStream = new PipedOutputStream(pipedInputStream);
			 var writer = new CoursesStAXWriter(pipedOutputStream)) {
			writer.write(courses);
		}
		var formattedXML = XMLFormatter.format(pipedInputStream);
		pipedInputStream.close();
		outputStream.write(formattedXML.getBytes(StandardCharsets.UTF_8));
	}

	public static void main(String[] args) throws Exception {
		if(args.length < 1) {
			throw new IllegalArgumentException("Too less arguments provided.");
		}
		if(args.length > 2) {
			throw new IllegalArgumentException("Too many arguments provided.");
		}
		var inputXMLFilePath = Paths.get(args[0]);

		if(args.length == 2) {
			var schemaFilePath = args[1];
			System.out.printf("Validating file '%s' against '%s'...\n", inputXMLFilePath, schemaFilePath);
			Validation.validate(new File(inputXMLFilePath.toString()), new File(schemaFilePath));
			System.out.println("Validation successful.");
		}

		var staxOutFilePath = Paths.get("output.stax.xml");
		runExample(new CoursesStAXParser(), inputXMLFilePath, staxOutFilePath, Course.NAME_COMPARATOR);
		System.out.println("StAX parsing completed. Saved to: " + staxOutFilePath);

		var domOutFilePath = Paths.get("output.dom.xml");
		runExample(new CoursesDOMParser(), inputXMLFilePath, domOutFilePath, Course.START_TIME_COMPARATOR);
		System.out.println("DOM parsing completed. Saved to: " + domOutFilePath);

		var saxOutFilePath = Paths.get("output.sax.xml");
		runExample(new CoursesSAXParser(), inputXMLFilePath, saxOutFilePath,
				Course.DISCIPLINE_COUNT_COMPARATOR.reversed());
		System.out.println("SAX Parsing completed. Saved to: " + saxOutFilePath);
	}
}
