package com.epam.rd.java.basic.task8.xml.writer;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerException;

public class XMLFormatter {
    private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();

    public static String format(InputStream is) throws TransformerException {
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        StreamSource streamSource = new StreamSource(is);
        StringWriter writer = new StringWriter();
        transformer.transform(streamSource, new StreamResult(writer));

        return writer.toString();
    }
}
