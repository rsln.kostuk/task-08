package com.epam.rd.java.basic.task8.xml.writer;

import com.epam.rd.java.basic.task8.courses.Course;
import com.epam.rd.java.basic.task8.courses.Discipline;
import com.epam.rd.java.basic.task8.courses.Person;

import static com.epam.rd.java.basic.task8.xml.CoursesConstants.*;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.XMLStreamException;

import java.io.OutputStream;
import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/*
Cursor API based courses xml writer
 */
public class CoursesStAXWriter implements CoursesXMLWriter, AutoCloseable {
    static final XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
    final XMLStreamWriter xmlStreamWriter;

    public CoursesStAXWriter(OutputStream os) throws XMLStreamException {
        xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(os);
    }

    @Override
    public void close() {
        try {
            xmlStreamWriter.close();
        } catch(XMLStreamException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void write(List<Course> courses) throws XMLStreamException {
        xmlStreamWriter.writeStartDocument("UTF-8", "1.0");
        xmlStreamWriter.writeStartElement(TARGET_NAMESPACE, ElementNames.Course.COURSES, NAMESPACE_URI);
        xmlStreamWriter.writeAttribute("xmlns:" + TARGET_NAMESPACE, NAMESPACE_URI);
        for(var course : courses) {
            writeCourse(course);
        }
        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();
        xmlStreamWriter.flush();
    }

    protected void writeCourse(Course course) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(ElementNames.Course.COURSE);

        writeSimpleElement(ElementNames.Course.NAME, course.name());
        writeIfPresent(ElementNames.Course.START_TIME, course.startTime().map(CoursesStAXWriter::formatDateTime));
        writeIfPresent(ElementNames.Course.END_TIME, course.endTime().map(CoursesStAXWriter::formatDateTime));

        xmlStreamWriter.writeStartElement(ElementNames.Discipline.DISCIPLINES);
        for(var discipline : course.disciplines()) {
            writeDiscipline(discipline);
        }
        xmlStreamWriter.writeEndElement();

        xmlStreamWriter.writeStartElement(ElementNames.Author.AUTHORS);
        for(var person : course.authors()) {
            writePerson(person);
        }
        xmlStreamWriter.writeEndElement();

        xmlStreamWriter.writeEndElement();
    }

    protected static String formatDateTime(LocalDateTime dateTime) {
        final String formatPattern = "yyyy-MM-dd HH:mm:ss";
        return dateTime.format(DateTimeFormatter.ofPattern(formatPattern))
                .replace(" ", "T");
    }

    protected void writeSimpleElement(String elementName, String text) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(elementName);
        xmlStreamWriter.writeCharacters(text);
        xmlStreamWriter.writeEndElement();
    }

    /*
    Its optional parameter is OK. This method is designed to facilitate optional usage.
     */
    protected void writeIfPresent(String elementName, Optional<String> text) throws XMLStreamException {
        if(text.isPresent()) {
            writeSimpleElement(elementName, text.get());
        }
    }

    protected void writeDiscipline(Discipline discipline) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(ElementNames.Discipline.DISCIPLINE);
        writeSimpleElement(ElementNames.Discipline.NAME, discipline.name());
        writeSimpleElement(ElementNames.Discipline.PRACTICES_COUNT, String.valueOf(discipline.practicesCount()));
        writeSimpleElement(ElementNames.Discipline.FINAL_CONTROL, discipline.control().name().toLowerCase());
        xmlStreamWriter.writeEndElement();
    }

    protected void writePerson(Person person) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(ElementNames.Author.AUTHOR);
        writeSimpleElement(ElementNames.Author.NAME, person.name());
        writeSimpleElement(ElementNames.Author.EMAIL, person.email());
        xmlStreamWriter.writeEndElement();
    }
}
