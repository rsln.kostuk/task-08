package com.epam.rd.java.basic.task8.xml.validation;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

public class Validation {
    public static void validate(File xmlFile, File xsdSchemaFile) throws SAXException,
            IOException {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(xsdSchemaFile);
        Validator validator = schema.newValidator();
        Source xmlFileSource = new StreamSource(xmlFile);
        validator.validate(xmlFileSource);
    }
}
