package com.epam.rd.java.basic.task8.xml.parser.stax;

import com.epam.rd.java.basic.task8.courses.Course;
import com.epam.rd.java.basic.task8.courses.Discipline;
import com.epam.rd.java.basic.task8.courses.Person;
import com.epam.rd.java.basic.task8.xml.parser.AbstractCoursesXMLParser;

import static com.epam.rd.java.basic.task8.xml.CoursesConstants.*;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.time.LocalDateTime;

public class CoursesStAXParser extends AbstractCoursesXMLParser {
    @Override
    public List<Course> parse(InputStream xmlInputStream) throws IOException, XMLStreamException {
        try(var processor = new StAXStreamProcessor(xmlInputStream)) {
            if(processor.iterateTo(ElementNames.Course.COURSES, null)) {
                while(processor.iterateTo(ElementNames.Course.COURSE, ElementNames.Course.COURSES)) {
                    parseCourse(processor);
                }
            }
        }
        return finalizeCoursesResult();
    }

    protected void parseCourse(StAXStreamProcessor processor) throws XMLStreamException {
        var course = new Course();
        // Reading name
        if(processor.iterateTo(ElementNames.Course.NAME, ElementNames.Course.COURSE)) {
            course.setName(processor.text());
        }
        // Reading optional startTime and endTime
        if(processor.iterateTo(XMLEvent.START_ELEMENT, ElementNames.Course.START_TIME, XMLEvent.START_ELEMENT,
                ElementNames.Discipline.DISCIPLINES)) {
            course.setStartTime(LocalDateTime.parse(processor.text()));
        }
        if(processor.iterateTo(XMLEvent.START_ELEMENT, ElementNames.Course.END_TIME, XMLEvent.START_ELEMENT,
                ElementNames.Discipline.DISCIPLINES)) {
            course.setEndTime(LocalDateTime.parse(processor.text()));
        }
        // Reading disciplines
        if(processor.iterateTo(ElementNames.Discipline.DISCIPLINES, ElementNames.Course.COURSE)) {
            while(processor.iterateTo(ElementNames.Discipline.DISCIPLINE, ElementNames.Discipline.DISCIPLINES)) {
                parseDiscipline(processor, course);
            }
        }
        // Reading authors
        if(processor.iterateTo(ElementNames.Author.AUTHORS, ElementNames.Course.COURSE)) {
            while(processor.iterateTo(ElementNames.Author.AUTHOR, ElementNames.Author.AUTHORS)) {
                parseAuthor(processor, course);
            }
        }
        courses.add(course);
    }

    protected void parseDiscipline(StAXStreamProcessor processor, Course course) throws XMLStreamException {
        String disciplineName = null;
        int practicesCount = 0;
        Discipline.FinalControl finalControl = null;
        if(processor.iterateTo(ElementNames.Discipline.NAME, ElementNames.Discipline.DISCIPLINE)) {
            disciplineName = processor.text();
        }
        if(processor.iterateTo(ElementNames.Discipline.PRACTICES_COUNT, ElementNames.Discipline.DISCIPLINE)) {
            practicesCount = Integer.parseInt(processor.text());
        }
        if(processor.iterateTo(ElementNames.Discipline.FINAL_CONTROL, ElementNames.Discipline.DISCIPLINE)) {
            finalControl = Discipline.FinalControl.valueOf(processor.text().toUpperCase());
        }
        course.addDiscipline(new Discipline(disciplineName, practicesCount, finalControl));
    }

    protected void parseAuthor(StAXStreamProcessor processor, Course course) throws XMLStreamException {
        String authorName = null;
        if(processor.iterateTo(ElementNames.Author.NAME, ElementNames.Author.AUTHOR)) {
            authorName = processor.text();
        }
        String email = null;
        if(processor.iterateTo(ElementNames.Author.EMAIL, ElementNames.Author.AUTHOR)) {
            email = processor.text();
        }
        course.addAuthor(new Person(authorName, email));
    }
}












