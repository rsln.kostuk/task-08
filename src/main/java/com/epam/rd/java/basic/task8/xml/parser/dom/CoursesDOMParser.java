package com.epam.rd.java.basic.task8.xml.parser.dom;

import com.epam.rd.java.basic.task8.courses.Course;
import com.epam.rd.java.basic.task8.courses.Discipline;
import com.epam.rd.java.basic.task8.courses.Person;
import com.epam.rd.java.basic.task8.xml.parser.AbstractCoursesXMLParser;

import static com.epam.rd.java.basic.task8.xml.CoursesConstants.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.time.LocalDateTime;

import org.w3c.dom.Node;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class CoursesDOMParser extends AbstractCoursesXMLParser {
    private static final String JAXP_SCHEMA_LANGUAGE =
            "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    private static final String W3C_XML_SCHEMA =
            "http://www.w3.org/2001/XMLSchema";

    // May be used to validate xml document
    private static final String JAXP_SCHEMA_SOURCE =
            "http://java.sun.com/xml/jaxp/properties/schemaSource";

    private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    static {
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setValidating(true);
        documentBuilderFactory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
    }

    @Override
    public List<Course> parse(InputStream xmlInputStream) throws ParserConfigurationException, IOException, SAXException {
        var documentBuilder = documentBuilderFactory.newDocumentBuilder();
        var document = documentBuilder.parse(xmlInputStream);
        var courseNodeList = document.getElementsByTagName(ElementNames.Course.COURSE);
        for(int i = 0; i < courseNodeList.getLength(); i++) {
            var courseNode = courseNodeList.item(i);
            parseCourse(courseNode);
        }
        return finalizeCoursesResult();
    }

    protected void parseCourse(Node courseNode) {
        var courseElementName = ElementNames.Course.COURSE;
        if(!courseNode.getNodeName().equals(courseElementName)) {
            throw new IllegalArgumentException(wrongElementMsg(courseElementName, courseNode.getNodeName()));
        }
        Course course = new Course();
        var courseChildNodes = courseNode.getChildNodes();
        for(int i = 0; i < courseChildNodes.getLength(); i++) {
            var currentNode = courseChildNodes.item(i);
            switch(currentNode.getNodeName()) {
                case ElementNames.Course.NAME:
                        course.setName(currentNode.getTextContent());
                        break;
                case ElementNames.Course.START_TIME:
                        course.setStartTime(LocalDateTime.parse(currentNode.getTextContent()));
                        break;
                case ElementNames.Course.END_TIME:
                        course.setEndTime(LocalDateTime.parse(currentNode.getTextContent()));
                        break;
                case ElementNames.Discipline.DISCIPLINES:
                        parseDisciplines(currentNode, course);
                        break;
                case ElementNames.Author.AUTHORS:
                        parseAuthors(currentNode, course);
                        break;
            }
        }
        courses.add(course);
    }

    protected void parseDisciplines(Node disciplinesNode, Course course) {
        var disciplinesElementName = ElementNames.Discipline.DISCIPLINES;
        if(!disciplinesNode.getNodeName().equals(disciplinesElementName)) {
            throw new IllegalArgumentException(wrongElementMsg(disciplinesElementName, disciplinesNode.getNodeName()));
        }
        var disciplinesNodeList = disciplinesNode.getChildNodes();
        for(int i = 0; i < disciplinesNodeList.getLength(); i++) {
            var currentNode = disciplinesNodeList.item(i);
            if(currentNode.getNodeName().equals(ElementNames.Discipline.DISCIPLINE)) {
                course.addDiscipline(parseDiscipline(currentNode));
            }
        }
    }

    protected Discipline parseDiscipline(Node disciplineNode) {
        var disciplineElementName = ElementNames.Discipline.DISCIPLINE;
        if(!disciplineNode.getNodeName().equals(disciplineElementName)) {
            throw new IllegalArgumentException(wrongElementMsg(disciplineElementName, disciplineNode.getNodeName()));
        }
        String name = null;
        int practicesCount = 0;
        Discipline.FinalControl finalControl = null;
        var disciplineChildNodes = disciplineNode.getChildNodes();
        for(int i = 0; i < disciplineChildNodes.getLength(); i++) {
            var currentNode = disciplineChildNodes.item(i);
            switch(currentNode.getNodeName()) {
                case ElementNames.Discipline.NAME:
                        name = currentNode.getTextContent();
                        break;
                case ElementNames.Discipline.PRACTICES_COUNT:
                        practicesCount = Integer.parseInt(currentNode.getTextContent());
                        break;
                case ElementNames.Discipline.FINAL_CONTROL:
                        finalControl = Discipline.FinalControl.valueOf(currentNode.getTextContent().toUpperCase());
                        break;
            }
        }
        return new Discipline(name, practicesCount, finalControl);
    }

    protected void parseAuthors(Node authorsNode, Course course) {
        var authorsElementName = ElementNames.Author.AUTHORS;
        if(!authorsNode.getNodeName().equals(authorsElementName)) {
            throw new IllegalArgumentException(wrongElementMsg(authorsElementName, authorsNode.getNodeName()));
        }
        var authorsNodeList = authorsNode.getChildNodes();
        for(int i = 0; i < authorsNodeList.getLength(); i++) {
            var currentNode = authorsNodeList.item(i);
            if(currentNode.getNodeName().equals(ElementNames.Author.AUTHOR)) {
                course.addAuthor(parseAuthor(currentNode));
            }
        }
    }

    protected Person parseAuthor(Node authorNode) {
        var authorElementName = ElementNames.Author.AUTHOR;
        if(!authorNode.getNodeName().equals(authorElementName)) {
            throw new IllegalArgumentException(wrongElementMsg(authorElementName, authorNode.getNodeName()));
        }
        String name = null;
        String email = null;
        var authorChildNodes = authorNode.getChildNodes();
        for(int i = 0; i < authorChildNodes.getLength(); i++) {
            var currentNode = authorChildNodes.item(i);
            switch(currentNode.getNodeName()) {
                case ElementNames.Author.NAME:
                    name = currentNode.getTextContent();
                    break;
                case ElementNames.Author.EMAIL:
                    email = currentNode.getTextContent();
                    break;
            }
        }
        return new Person(name, email);
    }

    protected String wrongElementMsg(String expectedElementName, String actualElementName) {
        return String.format("Wrong element name! Expected: '%s', actual: '%s'", expectedElementName,
                actualElementName);
    }
}












