package com.epam.rd.java.basic.task8.courses;

import java.util.Objects;

public class Person {
    private final String name;
    private final String email;

    public Person(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name) && email.equals(person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email);
    }

    public String name() {
        return name;
    }

    public String email() {
        return email;
    }
}
