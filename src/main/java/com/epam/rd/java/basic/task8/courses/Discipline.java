package com.epam.rd.java.basic.task8.courses;

import java.util.Objects;

public class Discipline {
    public enum FinalControl {
        TEST,
        EXAM,
        INTERVIEW
    }

    private final String name;
    private final int practicesCount;
    private final FinalControl control;

    public Discipline(String name, int practicesCount, FinalControl control) {
        this.name = name;
        this.practicesCount = practicesCount;
        this.control = control;
    }

    @Override
    public String toString() {
        return "Discipline{" +
                "name='" + name + '\'' +
                ", practicesCount=" + practicesCount +
                ", control=" + control +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Discipline that = (Discipline) o;
        return practicesCount == that.practicesCount && name.equals(that.name) && control == that.control;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, practicesCount, control);
    }

    public String name() {
        return name;
    }

    public int practicesCount() {
        return practicesCount;
    }

    public FinalControl control() {
        return control;
    }
}
