package com.epam.rd.java.basic.task8.courses;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

public class Course {
    public static final Comparator<Course> NAME_COMPARATOR = Comparator.comparing(Course::name);
    public static final Comparator<Course> START_TIME_COMPARATOR = (first, second) -> {
        var firstStartTime = first.startTime;
        var secondStartTime = second.startTime;
        if(firstStartTime == null || secondStartTime == null) {
            if(firstStartTime == null && secondStartTime == null) {
                return 0;
            }
            if(secondStartTime == null) {
                return -1;
            }
            return 1;
        }
        return first.startTime.compareTo(second.startTime);
    };
    public static final Comparator<Course> DISCIPLINE_COUNT_COMPARATOR =
            Comparator.comparingInt(course -> course.disciplines.size());

    private String name;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private final Set<Discipline> disciplines = new HashSet<>();
    private final Set<Person> authors = new HashSet<>();

    @Override
    public String toString() {
        return "Course[name: " + name + ", startTime: " + startTime + ", endTime: " + endTime + ", disciplines: "
            + disciplines + ", authors: " + authors + "]";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartTime(LocalDateTime dateTime) {
        startTime = dateTime;
    }

    public void setEndTime(LocalDateTime dateTime) {
        endTime = dateTime;
    }

    public void addDiscipline(Discipline discipline) {
        disciplines.add(discipline);
    }

    public void addAuthor(Person author) {
        authors.add(author);
    }

    public String name() {
        return name;
    }

    public Optional<LocalDateTime> startTime() {
        if(startTime == null) {
            return Optional.empty();
        }
        return Optional.of(startTime);
    }

    public Optional<LocalDateTime> endTime() {
        if(endTime == null) {
            return Optional.empty();
        }
        return Optional.of(endTime);
    }

    public Set<Discipline> disciplines() {
        return disciplines;
    }

    public Set<Person> authors() {
        return authors;
    }
}
