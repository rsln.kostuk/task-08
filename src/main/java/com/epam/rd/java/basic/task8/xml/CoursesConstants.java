package com.epam.rd.java.basic.task8.xml;

public interface CoursesConstants {
    String NAMESPACE_URI = "http://com.my.ns";
    String TARGET_NAMESPACE = "tns";

    /*
    Class ElementNames and its nested classes represent element names constants.
    These classes shouldn't be instantiated.
     */
    interface ElementNames {
        interface Course {
            String COURSES = "courses",
                    COURSE = "course",
                    NAME = "name",
                    START_TIME = "startTime",
                    END_TIME = "endTime";
        }

        interface Discipline {
            String DISCIPLINES = "disciplines",
                    DISCIPLINE = "discipline",
                    NAME = "disciplineName",
                    PRACTICES_COUNT = "practicesCount",
                    FINAL_CONTROL = "finalControl";
        }

        interface Author {
            String AUTHORS = "authors",
                    AUTHOR = "author",
                    NAME = "authorName",
                    EMAIL = "email";
        }
    }
}
