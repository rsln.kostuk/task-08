package com.epam.rd.java.basic.task8.xml.writer;

import com.epam.rd.java.basic.task8.courses.Course;

import javax.xml.stream.XMLStreamException;
import java.util.List;

public interface CoursesXMLWriter {
    void write(List<Course> courses) throws XMLStreamException;
}
