package com.epam.rd.java.basic.task8.xml.parser.stax;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import java.io.InputStream;
import java.util.Optional;

/*
    Low-level StAX wrapper
*/
public class StAXStreamProcessor implements AutoCloseable {
    private static final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private final XMLStreamReader xmlStreamReader;

    public StAXStreamProcessor(InputStream is) throws XMLStreamException {
        xmlStreamReader = xmlInputFactory.createXMLStreamReader(is);
    }

    @Override
    public void close() {
        if(xmlStreamReader != null) {
            try {
                xmlStreamReader.close();
            } catch(XMLStreamException ex) {
                System.err.println("Couldn't close xmlStreamReader: " + ex.getMessage());
            }
        }
    }

    public XMLStreamReader reader() {
        return xmlStreamReader;
    }

    public boolean iterateTo(String desiredElementLocalName, String parentName) throws XMLStreamException {
        return iterateTo(XMLEvent.START_ELEMENT, desiredElementLocalName, XMLEvent.END_ELEMENT, parentName);
    }

    /**
     * Calls iterateTo with stopEvent parameter equal to XMLEvent.END_ELEMENT.
     */
    public boolean iterateTo(int desiredEvent, String desiredLocalName, String parentName) throws XMLStreamException {
        return iterateTo(desiredEvent, desiredLocalName, XMLEvent.END_ELEMENT, parentName);
    }

    /**
     * Iterates through the document until desiredEvent is met and desiredLocalName equals to current element local
     * name.
     * Checks xmlStreamReader's state before each iteration.
     * @param stopEvent must be either START_ELEMENT, STOP_ELEMENT or ENTITY_REFERENCE,
     *                  so xmlStreamReader.getLocalName() shouldn't throw
     * @param desiredEvent has the same limitations as a stopEvent.
     * @return true, if current event equals to desiredEvent and current local name equals to desiredLocalName;
     *  false, if stopEvent of stopLocalName is met or if no events left.
     */
    public boolean iterateTo(int desiredEvent, String desiredLocalName, int stopEvent, String stopLocalName)
            throws XMLStreamException {
        // The event type of the current event should be examined first(before calling xmlStreamReader.next()),
        // cause the iterator can point on the element we're currently looking for.
        // It's not always necessary, and I'm not completely sure, but it seems quite reasonable.
        while(true) {
            int eventType = xmlStreamReader.getEventType();
            if(eventType == stopEvent && xmlStreamReader.getLocalName().equals(stopLocalName)) {
                return false;
            }
            if(eventType == desiredEvent && xmlStreamReader.getLocalName().equals(desiredLocalName)) {
                return true;
            }
            // After checking the event type and local name, the iterator can be moved forward.
            if(!xmlStreamReader.hasNext()) {
                break;
            }
            xmlStreamReader.next();
        }
        return false;
    }

    public Optional<String> attribute(String namespaceUri, String name) {
        String result = xmlStreamReader.getAttributeValue(namespaceUri, name);
        if(result != null) {
            return Optional.of(result);
        }
        return Optional.empty();
    }

    public Optional<String> attribute(String name) {
        return attribute(null, name);
    }

    public String text() throws XMLStreamException {
        return xmlStreamReader.getElementText();
    }
}
