package com.epam.rd.java.basic.task8.xml.parser;

import com.epam.rd.java.basic.task8.courses.Course;

import javax.xml.stream.XMLStreamException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.xml.sax.SAXException;

public interface CoursesXMLParser {
    List<Course> parse(InputStream xmlInputStream) throws IOException, XMLStreamException, ParserConfigurationException,
            SAXException;
}
