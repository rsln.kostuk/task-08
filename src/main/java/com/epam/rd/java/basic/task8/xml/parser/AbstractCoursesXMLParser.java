package com.epam.rd.java.basic.task8.xml.parser;

import com.epam.rd.java.basic.task8.courses.Course;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCoursesXMLParser implements CoursesXMLParser {
    protected List<Course> courses = new ArrayList<>();

    /**
     * Reallocates courses, so courses list is empty after this method call.
     * @return list of courses formed as a result of parsing.
     */
    protected List<Course> finalizeCoursesResult() {
        var result = courses;
        courses = new ArrayList<>();
        return result;
    }
}
